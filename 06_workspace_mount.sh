#!/usr/bin/with-contenv bash

# Functions that decode a JWT token
_decode_base64_url() {
  local len=$((${#1} % 4))
  local result="$1"
  if [ $len -eq 2 ]; then result="$1"'=='
  elif [ $len -eq 3 ]; then result="$1"'='
  fi
  echo "$result" | tr '_-' '/+' | base64 -d
}

# $1 => JWT to decode
# $2 => either 1 for header or 2 for body (default is 2)
decode_jwt() { _decode_base64_url $(echo -n $1 | cut -d "." -f ${2:-2}) | jq .; }

_workspace_scope="/d4science.research-infrastructures.eu"
echo "Manage the user's workspace"
workspace_dir='/workspace'
workspace_logdir='/var/log/workspace-lib'
workspace_libdir='/opt/workspace-lib'
workspace_fuse_jar="$workspace_libdir/fuse-workspace.jar"

[[ ! -d "$workspace_dir" ]] || [[ -z `ls -A "$workspace_dir"` ]] || mv $workspace_dir ${workspace_dir}.old

mkdir -p $workspace_dir
chown ${USER}:${USER} $workspace_dir
chown -R ${USER}:${USER} $workspace_logdir
chmod 444 $workspace_fuse_jar

_retval=
if [ -d /home/${USER}/workspace ]; then
  rmdir /home/${USER}/workspace
  _retval=$?
  if [ $_retval -ne 0 ]; then
      echo "The user has a directory named 'workspace' inside their home directory and it is not empty."
      echo "Not starting the workspace mount"
      exit $_retval
  fi
fi

echo "Mount the workspace"
su - "$USER" -c "/usr/bin/java  -cp .:${workspace_dir}:${workspace_logdir}/ -Dlogback.configurationFile=${workspace_logdir}/logback.xml -jar $workspace_fuse_jar $SHINYPROXY_OIDC_ACCESS_TOKEN ${_workspace_scope} $workspace_dir" >/dev/null 2>&1 &

_retval=
_fuse_process=$(ps auwwx | grep fuse | grep java)
_retval=$?
if [ $_retval -ne 0 ]; then
  echo "The mount of the workspace failed"
  exit 1
fi

ln -sf $workspace_dir /home/${USER}/workspace
exit 0
