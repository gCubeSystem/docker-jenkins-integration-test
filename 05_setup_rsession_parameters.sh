#!/usr/bin/with-contenv bash

echo "session-timeout-minutes=7200" >> /etc/rstudio/rsession.conf
echo "session-disconnected-timeout-minutes=1440" >> /etc/rstudio/rsession.conf
echo "session-quit-child-processes-on-exit=0" >> /etc/rstudio/rsession.conf
#echo "session-default-working-dir=~" >> /etc/rstudio/rsession.conf
#echo "session-default-new-project-dir=~" >> /etc/rstudio/rsession.conf
#echo "session-save-action-default=yes" >> /etc/rstudio/rsession.conf
echo "allow-shell=0" >> /etc/rstudio/rsession.conf
#echo "allow-terminal-websockets=1" >> /etc/rstudio/rsession.conf
echo "limit-cpu-time-minutes=0" >> /etc/rstudio/rsession.conf
echo "limit-file-upload-size-mb=0" >> /etc/rstudio/rsession.conf
#echo "limit-xfs-disk-quota=no" >> /etc/rstudio/rsession.conf
